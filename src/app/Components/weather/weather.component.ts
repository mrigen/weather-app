import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  url = 'https://api.openweathermap.org/data/2.5/weather?q=';
  api = 'appid=85d110eafe60abac819e84c1903c04ca#';
  weatherForm!:FormGroup
  cityName:any;
  country:any;
  temp: any;
  maxTemp: any;
  minTemp: any;
  humidity: any;

  constructor() {}

  ngOnInit(): void {
    this.weatherForm = new FormGroup({
      city: new FormControl('')
    })
  }

  submit() {
    console.log(this.weatherForm.value.city);
    
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/weather?q=${this.weatherForm.value.city}&units=metric&appid=85d110eafe60abac819e84c1903c04ca#`
      )
      .then((response) => {

        this.cityName = response.data.name
        this.temp = response.data.main.temp
        this.maxTemp = response.data.main.temp_max
        this.minTemp = response.data.main.temp_min
        this.humidity = response.data.main.humidity
        console.log(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
